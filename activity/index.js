// PART1
let firstName = "Jesus";
console.log("First Name: "+ firstName);
let lastName = "Carullo";
console.log("Last Name: "+ lastName);
let age = 28;
console.log("Age: " + age);
let hobbies = ["Watching", "Playing", "Reading"];
console.log("Hobbies: ");
console.log(hobbies);
let workAddress = {
    houseNumber: 6,
    street: "San Leon St.",
    city: "Quezon City",
    state: "Metro Manila"
}
console.log("Work Address: ");
console.log(workAddress);

// PART2

let fullName = "Steve Rogers";
console.log("My full name is: " + fullName);

let currentAge = 40;
console.log("My current age is: " + currentAge);

let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
console.log("My Friends are: ")
console.log(friends);

let profile = {
    username: "captain_america",
    fullName: "Steve Rogers",
    age: 40,
    isActive: false
};
console.log("My Full Profile: ")
console.log(profile);

let fullNameFriend = "Bucky Barnes";
console.log("My bestfriend is: " + fullNameFriend);

const lastLocation = "Arctic Ocean";
console.log("I was found frozen in: " + lastLocation);
